version: "3.7"

services:
  postgres:
    image: postgres:13.1
    environment:
      POSTGRES_PASSWORD: secret
    volumes:
      - ./pg-init.sh:/docker-entrypoint-initdb.d/pg-init.sh
    networks:
      - intranet

  patients-api:
    build:
      context: ../
      dockerfile: build/Dockerfile
      target: patients-api
    environment:
      PG_CONNECTION_URL: postgres://patients:secret@postgres:5432/patients?sslmode=disable
    restart: unless-stopped
    networks:
      - intranet
    depends_on:
      - migrate

  stats-api:
    build:
      context: ../
      dockerfile: build/Dockerfile
      target: stats-api
    environment:
      PG_CONNECTION_URL: postgres://patients:secret@postgres:5432/patients?sslmode=disable
    restart: unless-stopped
    networks:
      - intranet

  migrate:
    build:
      context: ../
      dockerfile: build/Dockerfile
      target: migrate
    environment:
      PG_CONNECTION_URL: postgres://patients:secret@postgres:5432/patients?sslmode=disable
    restart: on-failure
    networks:
      - intranet
    depends_on:
      - postgres

  hydra:
    image: oryd/hydra:v1.9.0
    ports:
      - "4444:4444" # Public port
      - "4445:4445" # Admin port
      - "5555:5555" # Port for hydra token user
    command:
      serve -c /etc/config/hydra/hydra.yml all --dangerous-force-http
    volumes:
      -
        type: bind
        source: ./hydra.yml
        target: /etc/config/hydra/hydra.yml
    environment:
      - DSN=postgres://hydra:secret@postgres:5432/hydra?sslmode=disable&max_conns=20&max_idle_conns=4
    restart: unless-stopped
    depends_on:
      - hydra-migrate
    networks:
      - intranet

  hydra-migrate:
    image: oryd/hydra:v1.9.0
    environment:
      - DSN=postgres://hydra:secret@postgres:5432/hydra?sslmode=disable&max_conns=20&max_idle_conns=4
    command:
      migrate -c /etc/config/hydra/hydra.yml sql -e --yes
    volumes:
      -
        type: bind
        source: ./hydra.yml
        target: /etc/config/hydra/hydra.yml
    restart: on-failure
    networks:
      - intranet

  consent:
    environment:
      - HYDRA_ADMIN_URL=http://hydra:4445
    image: oryd/hydra-login-consent-node:v1.9.0
    restart: unless-stopped
    networks:
      - intranet

  oathkeeper:
    image: oryd/oathkeeper:v0.38.5-alpine
    command:
      serve --config=/etc/config/oathkeeper/config.yaml
    volumes:
      - type: bind
        source: ./oathkeeper
        target: /etc/config/oathkeeper
    ports:
      - "4455:4455"
      - "4456:4456"
    networks:
      - intranet

  swaggerui:
    image: swaggerapi/swagger-ui:v3.40.0
    environment:
      URL: ""
      URLS_PRIMARY_NAME: patients
      URLS: |
        [{ url: 'docs/patients.yaml', name: 'patients' },
         { url: 'docs/stats.yaml', name: 'stats' }] 
    volumes:
      - ./../api/:/usr/share/nginx/html/docs/
    networks:
      - intranet


networks:
  intranet: