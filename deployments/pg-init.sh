#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE DATABASE "patients";
    CREATE USER "patients" WITH PASSWORD 'secret';
    GRANT ALL PRIVILEGES ON DATABASE "patients" TO "patients";

    CREATE DATABASE "hydra";
    CREATE USER "hydra" WITH PASSWORD 'secret';
    GRANT ALL PRIVILEGES ON DATABASE "hydra" TO "hydra";
EOSQL