package sql

import (
	"context"
	"testing"

	_ "github.com/jackc/pgx/v4/stdlib"

	"github.com/stretchr/testify/suite"
	"gitlab.com/ppaysant/medical-demo/internal/domain"
	"gitlab.com/ppaysant/medical-demo/internal/repositories/sql"
)

type StatsTestSuite struct {
	TestSuite
}

func (suite *StatsTestSuite) TestEmpty() {
	require := suite.Require()
	ctx := context.Background()
	repo := sql.NewStatsRepository(suite.DB)

	aggs, err := repo.GetPatientsByDisease(ctx)
	require.NoError(err)
	require.Equal([]domain.Aggregate{}, aggs)
}

func (suite *StatsTestSuite) TestNonEmpty() {
	require := suite.Require()
	require.NoError(suite.LoadFixtures("testdata/fixtures"))
	ctx := context.Background()
	repo := sql.NewStatsRepository(suite.DB)
	expAggs := []domain.Aggregate{
		{Label: "Gastroenteritis", Value: 2},
		{Label: "SARS-CoV-2", Value: 1},
	}

	aggs, err := repo.GetPatientsByDisease(ctx)
	require.NoError(err)
	require.Equal(expAggs, aggs)
}

func TestStats(t *testing.T) {
	suite.Run(t, new(StatsTestSuite))
}
