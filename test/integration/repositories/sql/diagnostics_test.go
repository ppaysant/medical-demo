package sql

import (
	"context"
	"testing"
	"time"

	_ "github.com/jackc/pgx/v4/stdlib"

	"github.com/stretchr/testify/suite"
	"gitlab.com/ppaysant/medical-demo/internal/domain"
	"gitlab.com/ppaysant/medical-demo/internal/repositories/sql"
)

type DiagnosticsTestSuite struct {
	TestSuite
}

func (suite *DiagnosticsTestSuite) TestEmpty() {
	require := suite.Require()
	ctx := context.Background()
	repo := sql.NewPatientsRepository(suite.DB)

	diags, err := repo.GetAllPatientsDiagnostics(ctx, "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11")
	require.NoError(err)
	require.Equal([]domain.Diagnostic{}, diags)
}

func (suite *DiagnosticsTestSuite) TestPatientWithDiagnostics() {
	require := suite.Require()
	require.NoError(suite.LoadFixtures("testdata/fixtures"))
	ctx := context.Background()
	repo := sql.NewPatientsRepository(suite.DB)
	patientID := "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11"
	expectedDiags := []domain.Diagnostic{
		{
			ID:      "62f9bf49-3a71-4b68-bb78-adcea78e3361",
			Disease: "SARS-CoV-2",
			MadeOn:  time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC),
		},
		{
			ID:      "62f9bf49-3a71-4b68-bb78-adcea78e3362",
			Disease: "Gastroenteritis",
			MadeOn:  time.Date(2020, 5, 12, 0, 0, 0, 0, time.UTC),
		},
	}

	diags, err := repo.GetAllPatientsDiagnostics(ctx, patientID)
	require.NoError(err)
	require.Equal(expectedDiags, diags)

	newdiag := domain.Diagnostic{
		Disease: "Foobar",
		MadeOn:  time.Date(2021, 1, 25, 0, 0, 0, 0, time.UTC),
	}

	require.NoError(repo.CreatePatientsDiagnostic(ctx, patientID, &newdiag))
	require.NotZero(newdiag.ID)

	diags, err = repo.GetAllPatientsDiagnostics(ctx, patientID)
	require.NoError(err)
	require.Equal(append(expectedDiags, newdiag), diags)

}

func TestDiagnostics(t *testing.T) {
	suite.Run(t, new(DiagnosticsTestSuite))
}
