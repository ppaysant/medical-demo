package sql

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"strings"
	"time"

	gosql "database/sql"

	"github.com/go-testfixtures/testfixtures/v3"
	"github.com/jackc/pgx/v4/pgxpool"
	_ "github.com/jackc/pgx/v4/stdlib"

	"github.com/ory/dockertest/v3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/ppaysant/medical-demo/internal/domain"
	"gitlab.com/ppaysant/medical-demo/internal/repositories/sql"
)

const pgVersion = "13.1"

type TestSuite struct {
	suite.Suite
	DB        *pgxpool.Pool
	Container *dockertest.Resource
}

const databaseName = "medical-demo-integration-test"
const databaseUser = "integration-test"
const databasePassword = "secret"

func (suite *TestSuite) TestEmptyRepository() {
	require := suite.Require()
	ctx := context.Background()
	repo := sql.NewPatientsRepository(suite.DB)

	patients, err := repo.GetAllPatients(ctx)
	require.NoError(err)
	require.Equal([]domain.Patient{}, patients)

	ok, _, err := repo.GetPatientByID(ctx, "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11")
	require.NoError(err)
	require.False(ok)
}

func (suite *TestSuite) SetupTest() {
	suite.Require().NoError(suite.setupTestOrError())
}

func (suite *TestSuite) setupTestOrError() error {
	ctx := context.Background()
	pool, err := dockertest.NewPool("")
	if err != nil {
		return fmt.Errorf("Failed to create docker pool: %w", err)
	}
	pgc, err := newPostgresContainer(pool)

	if err != nil {
		return fmt.Errorf("Failed to create postgres container: %w", err)
	}
	suite.Container = pgc
	if err := tcpWait(pgHostPort(pgc), 5*time.Second); err != nil {
		return fmt.Errorf("Postgres not reachable: %w", err)
	}
	time.Sleep(5 * time.Second) // wait for postgres to be ready TOOD: find a better way
	pgdb, err := pgxpool.Connect(ctx, pgURL(pgc, "postgres", "postgres", "postgres"))
	if err != nil {
		return fmt.Errorf("Failed to connect postgres: %w", err)
	}
	defer pgdb.Close()
	if err := createDatabase(pgdb); err != nil {
		return fmt.Errorf("Failed to create database: %w", err)
	}
	dburl := pgURL(pgc, databaseUser, databasePassword, databaseName)
	db, err := pgxpool.Connect(ctx, dburl)
	if err != nil {
		return fmt.Errorf("Failed to connect postgres: %w", err)
	}
	if err := sql.Migrate(ctx, dburl); err != nil {
		return fmt.Errorf("Failed to migrate: %w", err)
	}
	suite.DB = db
	return nil
}

func (suite *TestSuite) TearDownTest() {
	suite.DB.Close()
	if suite.Container != nil {
		suite.Container.Close()
	}
	// TODO: cleanup docker
}

func (suite *TestSuite) LoadFixtures(path string) error {
	db, err := gosql.Open("pgx", suite.DB.Config().ConnString())
	if err != nil {
		return fmt.Errorf("Failed to open database for fixtures: %w", err)
	}
	fixtures, err := testfixtures.New(
		testfixtures.Database(db),
		testfixtures.Dialect("postgres"),
		testfixtures.Directory(path),
		testfixtures.UseAlterConstraint(),
	)
	if err != nil {
		return fmt.Errorf("Failed to create fixtures: %w", err)
	}
	if err := fixtures.Load(); err != nil {
		return fmt.Errorf("Failed to load fixtures: %w", err)
	}
	return nil
}

func createDatabase(db *pgxpool.Pool) error {
	ctx := context.Background()
	if _, err := db.Exec(ctx, fmt.Sprintf("create database %s", quoteIdentifier(databaseName))); err != nil {
		return err
	}
	if _, err := db.Exec(ctx,
		fmt.Sprintf("create user %s with password '%s'", quoteIdentifier(databaseUser), databasePassword)); err != nil {
		return fmt.Errorf("Failed to create user: %w", err)
	}

	return nil
}

func pgHostPort(resource *dockertest.Resource) string {
	// TODO: handle different hostname when docker is not on localhost
	return net.JoinHostPort("localhost", resource.GetPort("5432/tcp"))
}

func pgURL(resource *dockertest.Resource, user, password, database string) string {
	return fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=disable",
		user,
		password,
		pgHostPort(resource),
		database,
	)
}

func newPostgresContainer(pool *dockertest.Pool) (*dockertest.Resource, error) {
	res, err := pool.Run("postgres", pgVersion, []string{
		"POSTGRES_PASSWORD=postgres",
	})
	if err != nil {
		return nil, err
	}
	if err := res.Expire(60); err != nil {
		return nil, err
	}
	return res, nil
}

func quoteIdentifier(s string) string {
	return `"` + strings.ReplaceAll(s, `"`, `""`) + `"`
}

func tcpWait(host string, timeout time.Duration) error {
	startedAt := time.Now()
	for ellapsed := time.Duration(0); ellapsed < timeout; ellapsed = time.Now().Sub(startedAt) {
		conn, err := net.DialTimeout("tcp", host, timeout-ellapsed)
		if err == nil {
			log.Printf("Waited %s", ellapsed)
			return conn.Close()
		}
	}
	return errors.New("Timeout")
}
