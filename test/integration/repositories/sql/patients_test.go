package sql

import (
	"context"
	"testing"
	"time"

	_ "github.com/jackc/pgx/v4/stdlib"

	"github.com/stretchr/testify/suite"
	"gitlab.com/ppaysant/medical-demo/internal/domain"
	"gitlab.com/ppaysant/medical-demo/internal/repositories/sql"
)

type PatientsTestSuite struct {
	TestSuite
}

func (suite *PatientsTestSuite) TestEmpty() {
	require := suite.Require()
	ctx := context.Background()
	repo := sql.NewPatientsRepository(suite.DB)

	patients, err := repo.GetAllPatients(ctx)
	require.NoError(err)
	require.Equal([]domain.Patient{}, patients)

	ok, _, err := repo.GetPatientByID(ctx, "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11")
	require.NoError(err)
	require.False(ok)
}

func (suite *PatientsTestSuite) TestNonEmpty() {
	require := suite.Require()
	require.NoError(suite.LoadFixtures("testdata/fixtures"))
	ctx := context.Background()
	repo := sql.NewPatientsRepository(suite.DB)

	patients, err := repo.GetAllPatients(ctx)
	require.NoError(err)
	require.Equal([]domain.Patient{
		{
			ID:        "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a11",
			Name:      "John Doe",
			Address:   "123 Main St\nAnytown, USA",
			Birthdate: time.Date(1984, 1, 1, 0, 0, 0, 0, time.UTC),
		},
		{
			ID:        "a0eebc99-9c0b-4ef8-bb6d-6bb9bd380a12",
			Name:      "Alice Roe",
			Address:   "32 High St\nAnothertown, USA",
			Birthdate: time.Date(2000, 5, 12, 0, 0, 0, 0, time.UTC),
		},
	}, patients)

	newpatient := domain.Patient{
		Name:      "Jack Bauer",
		Address:   "someaddress",
		Birthdate: time.Date(1966, 2, 18, 0, 0, 0, 0, time.UTC),
	}

	require.NoError(repo.CreatePatient(ctx, &newpatient))
	require.NotZero(newpatient.ID)

	require.NoError(repo.DeletePatientByID(ctx, newpatient.ID))
}

func TestPatients(t *testing.T) {
	suite.Run(t, new(PatientsTestSuite))
}
