# Medical demo

## Services

The application is composed of 3 services
* patients-api: patients management API
* stats-api: patients statistics API
* hydra: oauth authentication service securing access to APIs

## API endpoints

APIs are documented with OpenAPI:
* [patients-api](api/patients.yaml) 
* [stats-api](api/stats.yaml) 

## Configuration

Two postgresql databases and two users are required.

Databases:
* patients database
* hydra database

User:
* patients: required all privileges on patients database for maintenance tasks
* hydra: required all privileges on hydra database for maintenance tasks

## Testing

Unit tests: `make unit-test`

Integration tests: `make integration-test`. Requires `docker-compose`.

## Build

Localhost build requirements:
* GNU make
* go 1.15

Do build patients-api, run `make build`. `dist/bin/patients-api` binary is produced.

## Configuration

`PG_CONNECTION_URL` env var: pgx pool connection url. See https://pkg.go.dev/github.com/jackc/pgx/v4/pgxpool#ParseConfig

## Start local env

```
docker-compose -f deployments/docker-compose-dev.yml up --build
# Create swagger gui client
docker-compose -f deployments/docker-compose-dev.yml exec hydra \
    hydra clients create \
    --endpoint http://127.0.0.1:4445 \
    --id swaggerui \
    --secret secret \
    --grant-types authorization_code,refresh_token \
    --token-endpoint-auth-method client_secret_post \
    --response-types code,id_token \
    --callbacks http://swagger.localhost:4455/oauth2-redirect.html \
    --allowed-cors-origins http://swagger.localhost:4455
```

Open `http://swagger.localhost:4455` to access APIs live documentation.

Click Authorize:
* client_id: swaggerui
* client_secret: secret

## TODO
* CI
* metrics / observability
* kubernetes deployment
* separated micro services
* oauth scopes