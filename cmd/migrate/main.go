package main

import (
	"context"
	"log"
	"os"

	"gitlab.com/ppaysant/medical-demo/internal/repositories/sql"
)

func main() {
	if err := sql.Migrate(context.Background(), os.Getenv("PG_CONNECTION_URL")); err != nil {
		log.Fatalf("Migration failed: %v", err)
	}
}
