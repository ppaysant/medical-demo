package main

import (
	"context"
	"log"
	"os"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/labstack/echo/v4"
	emiddleware "github.com/labstack/echo/v4/middleware"
	"gitlab.com/ppaysant/medical-demo/internal/app/patients/handlers"
	"gitlab.com/ppaysant/medical-demo/internal/app/patients/handlers/middleware"
	"gitlab.com/ppaysant/medical-demo/internal/repositories/sql"
)

func main() {
	cfg, err := pgxpool.ParseConfig(os.Getenv("PG_CONNECTION_URL"))
	if err != nil {
		log.Fatalf("Failed to parse pg url: %v", err)
	}
	cfg.LazyConnect = true
	db, err := pgxpool.ConnectConfig(context.Background(), cfg)
	if err != nil {
		log.Fatalf("Error connecting to database: %v", err)
	}
	repo := sql.NewPatientsRepository(db)
	h := handlers.NewPatientsHandler(repo)
	e := echo.New()
	e.Use(emiddleware.Logger())
	e.Use(emiddleware.CORS())
	e.Use(middleware.OnlyJSON)
	handlers.RegisterHandlersWithBaseURL(e, h, "/v1")
	log.Fatal(e.Start(":1323"))
}
