package handlers

import (
	"fmt"
	"net/http"

	openapi_types "github.com/deepmap/oapi-codegen/pkg/types"
	"github.com/labstack/echo/v4"
	"gitlab.com/ppaysant/medical-demo/internal/domain"
	"gitlab.com/ppaysant/medical-demo/internal/ports"
)

type handler struct {
	repository ports.PatientsRepository
}

func (h *handler) ListPatients(ctx echo.Context) error {
	patientModels, err := h.repository.GetAllPatients(ctx.Request().Context())
	if err != nil {
		return fmt.Errorf("Error retrieving patients: %w", err)
	}
	patients := make([]Patient, len(patientModels))
	for i, m := range patientModels {
		patients[i] = fromPatientModel(m)
	}
	return ctx.JSON(http.StatusOK, patients)
}

func (h *handler) CreatePatient(ctx echo.Context) error {
	patient := Patient{}
	if err := ctx.Bind(&patient); err != nil {
		return ctx.JSON(http.StatusBadRequest, Error{
			Message: "Invalid JSON",
		})
	}
	patientModel := toPatientModel(patient)
	if err := h.repository.CreatePatient(ctx.Request().Context(), &patientModel); err != nil {
		return fmt.Errorf("Error creating patient: %w", err)
	}
	return ctx.JSON(http.StatusCreated, fromPatientModel(patientModel))
}

func (h *handler) GetPatientByID(ctx echo.Context, patientID string) error {
	ok, patient, err := h.repository.GetPatientByID(ctx.Request().Context(), patientID)
	if err != nil {
		return fmt.Errorf("Error retrieving patient: %w", err)
	}
	if !ok {
		return ctx.JSON(http.StatusNotFound, Error{
			Message: "patient not found",
		})
	}
	return ctx.JSON(http.StatusOK, fromPatientModel(patient))
}

func (h *handler) DeletePatientByID(ctx echo.Context, patientID string) error {
	err := h.repository.DeletePatientByID(ctx.Request().Context(), patientID)
	if err != nil {
		return fmt.Errorf("Failed to delete patien: %w", err)
	}
	return ctx.NoContent(http.StatusNoContent)
}

func (h *handler) ListPatientsDiagnostics(ctx echo.Context, patientID string) error {
	diagnosticModels, err := h.repository.GetAllPatientsDiagnostics(ctx.Request().Context(), patientID)
	if err != nil {
		return fmt.Errorf("Failed to retrieve diagnostics: %w", err)
	}
	diagnostics := make([]Diagnostic, len(diagnosticModels))
	for i, m := range diagnosticModels {
		diagnostics[i] = fromDiagnosticModel(m)
	}
	return ctx.JSON(http.StatusOK, diagnostics)
}

func (h *handler) CreatePatientsDiagnostic(ctx echo.Context, patientID string) error {
	diagnostic := Diagnostic{}
	if err := ctx.Bind(&diagnostic); err != nil {
		return ctx.JSON(http.StatusBadRequest, Error{
			Message: "Invalid JSON",
		})
	}
	diagnosticModel := toDiagnosticModel(diagnostic)
	if err := h.repository.CreatePatientsDiagnostic(ctx.Request().Context(), patientID, &diagnosticModel); err != nil {
		return fmt.Errorf("Error creating patient: %w", err)
	}
	return ctx.JSON(http.StatusCreated, fromDiagnosticModel(diagnosticModel))
}

func NewPatientsHandler(repository ports.PatientsRepository) ServerInterface {
	return &handler{
		repository: repository,
	}
}

func toPatientModel(patient Patient) domain.Patient {
	return domain.Patient{
		ID:        patient.Id,
		Name:      patient.Name,
		Address:   patient.Address,
		Birthdate: patient.Birthdate.Time,
	}
}

func fromPatientModel(patient domain.Patient) Patient {
	return Patient{
		Id:      patient.ID,
		Name:    patient.Name,
		Address: patient.Address,
		Birthdate: openapi_types.Date{
			Time: patient.Birthdate,
		},
	}
}

func toDiagnosticModel(diagnostic Diagnostic) domain.Diagnostic {
	return domain.Diagnostic{
		ID:      diagnostic.Id,
		Disease: diagnostic.Disease,
		MadeOn:  diagnostic.MadeOn.Time,
	}
}

func fromDiagnosticModel(diagnostic domain.Diagnostic) Diagnostic {
	return Diagnostic{
		Id:      diagnostic.ID,
		Disease: diagnostic.Disease,
		MadeOn: openapi_types.Date{
			Time: diagnostic.MadeOn,
		},
	}
}
