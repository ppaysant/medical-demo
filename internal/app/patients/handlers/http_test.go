package handlers_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/ppaysant/medical-demo/internal/app/patients/handlers"
	"gitlab.com/ppaysant/medical-demo/internal/domain"
	"gitlab.com/ppaysant/medical-demo/internal/mocks/ports"
)

func TestListPatients(t *testing.T) {
	t.Run("should_return_empty_list_when_no_patient", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()
		e := echo.New()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		rec := httptest.NewRecorder()
		c := e.NewContext(req, rec)
		repo := ports.NewMockPatientsRepository(ctrl)
		gomock.InOrder(
			repo.EXPECT().GetAllPatients(gomock.Any()).Return([]domain.Patient{}, nil),
		)
		h := handlers.NewPatientsHandler(repo)
		require.NoError(t, h.ListPatients(c))
		assert.Equal(t, http.StatusOK, rec.Code)
		assert.JSONEq(t, "[]", rec.Body.String())
	})
}
