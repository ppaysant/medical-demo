package middleware

import (
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"gitlab.com/ppaysant/medical-demo/internal/app/patients/handlers"
)

func OnlyJSON(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		ct := c.Request().Header.Get(echo.HeaderContentType)
		if ct == "" || strings.HasPrefix(ct, echo.MIMEApplicationJSON) {
			return next(c)
		}
		return c.JSON(http.StatusBadRequest, handlers.Error{
			Message: "Unsupported content type",
		})
	}
}
