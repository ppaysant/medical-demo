package handlers

import (
	"errors"
	"fmt"
	"io"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/wcharczuk/go-chart/v2"
	"gitlab.com/ppaysant/medical-demo/internal/domain"
	"gitlab.com/ppaysant/medical-demo/internal/ports"
)

type (
	handler struct {
		repository ports.StatsRepository
	}
	renderable interface {
		Render(rp chart.RendererProvider, w io.Writer) error
	}
)

const MIMEImageSVG = "image/svg+xml"

func (h *handler) GetPatientsByDisease(ctx echo.Context, format Format) error {
	aggregateModels, err := h.repository.GetPatientsByDisease(ctx.Request().Context())
	if err != nil {
		return fmt.Errorf("Failed to retrieve patients by disease: %w", err)
	}
	switch format {
	case Format_json:
		aggregates := make([]SingleAggregate, len(aggregateModels))
		for i, m := range aggregateModels {
			aggregates[i] = aggregateFromModel(m)
		}
		return ctx.JSON(http.StatusOK, aggregates)
	case Format_svg:
		values := make([]chart.Value, len(aggregateModels))
		for i, m := range aggregateModels {
			values[i] = chart.Value{
				Label: m.Label,
				Value: float64(m.Value),
			}
		}
		pie := chart.PieChart{
			Width:  512,
			Height: 512,
			Values: values,
		}
		return svgChart(ctx, pie)
	default:
		return ctx.JSON(http.StatusNotFound, Error{
			Message: "unsupported format",
		})
	}
}

func (h *handler) GetPatientsByDiseaseAndAge(ctx echo.Context, format Format) error {
	return errors.New("not implemented")
}

func NewStatsHandler(repository ports.StatsRepository) ServerInterface {
	return &handler{
		repository: repository,
	}
}

func aggregateFromModel(agg domain.Aggregate) SingleAggregate {
	return SingleAggregate{
		Label: agg.Label,
		Value: float32(agg.Value),
	}
}

func svgChart(ctx echo.Context, rend renderable) error {
	ctx.Response().WriteHeader(http.StatusOK)
	ctx.Response().Header().Set(echo.HeaderContentType, MIMEImageSVG)
	return rend.Render(chart.SVG, ctx.Response())
}
