package sql

import (
	"context"
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	bindata "github.com/golang-migrate/migrate/v4/source/go_bindata"

	// Load postgres driver
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	"gitlab.com/ppaysant/medical-demo/internal/repositories/sql/migrations"
)

const createPatientsTable = `create table "patients" (
		id uuid default gen_random_uuid(),
		name text
	)`

// Migrate migrates the database schema
func Migrate(ctx context.Context, databaseURL string) error {
	s := bindata.Resource(migrations.AssetNames(),
		func(name string) ([]byte, error) {
			return migrations.Asset(name)
		})

	d, err := bindata.WithInstance(s)
	if err != nil {
		return fmt.Errorf("Failed to load migrations: %w", err)
	}

	m, err := migrate.NewWithSourceInstance(
		"go-bindata",
		d,
		databaseURL)

	if err != nil {
		return fmt.Errorf("Failed to create migration: %w", err)
	}

	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		return fmt.Errorf("Migration failed: %w", err)
	}
	return nil
}
