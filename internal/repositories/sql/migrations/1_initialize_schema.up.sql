create table "patients" (
    id        uuid primary key default gen_random_uuid(),
    name      text not null,
    address   text not null,
    birthdate date not null
);

create table "diagnostics" (
    id         uuid primary key default gen_random_uuid(),
    patient_id uuid not null references patients(id),
    disease    text not null,
    made_on    date not null
)