package migrations

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"strings"
)

func bindata_read(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	return buf.Bytes(), nil
}

var __1_initialize_schema_down_sql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x4a\x29\xca\x2f\x50\x28\x49\x4c\xca\x49\x55\x50\x4a\xc9\x4c\x4c\xcf\xcb\x2f\x2e\xc9\x4c\x2e\x56\xb2\xe6\x42\x96\x29\x48\x2c\xc9\x4c\xcd\x2b\x29\x56\xb2\x06\x04\x00\x00\xff\xff\xcb\xae\x8d\x85\x30\x00\x00\x00")

func _1_initialize_schema_down_sql() ([]byte, error) {
	return bindata_read(
		__1_initialize_schema_down_sql,
		"1_initialize_schema.down.sql",
	)
}

var __1_initialize_schema_up_sql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x94\x8f\x3b\x6e\x03\x31\x0c\x44\xfb\x3d\x05\xe1\x6a\x0d\xe4\x06\x39\x8c\x40\x2f\xc7\x0e\x11\x89\x32\x48\x0a\x88\x6f\x1f\x78\x3f\x85\x01\xa7\x88\x0a\x35\x33\x0f\x7c\xb3\x38\x38\x41\xc9\x97\x0a\x3a\xdd\x39\x15\x96\x71\xa2\x79\x22\x22\x52\xa1\xfd\x8d\xa1\x42\x77\xd7\xc6\xfe\xa0\x6f\x3c\x48\x70\xe5\x51\x93\x6e\xb0\xe2\x6c\xd2\x5b\x79\x76\xe6\xf3\xc7\x4a\x1a\x37\x6c\x64\xe2\x27\xc9\x7a\x92\x8d\x5a\xb7\x90\x45\x1c\x11\xef\xc3\x8b\x7a\x7e\xc9\x53\x6a\xfd\x8e\x70\x3a\x7f\x4e\xd3\xab\xad\x28\xdf\xac\x47\xea\xf2\x46\xf8\xdf\xc6\xfb\xf6\xa2\xb2\xa1\xc7\x61\x72\x5c\xe1\xb0\x05\x71\x74\x62\x56\xd9\x29\xd1\x00\x07\xfe\x18\xda\x58\x50\xba\xad\xc5\xd7\x31\xbf\x01\x00\x00\xff\xff\xd2\xcd\xf7\x40\x78\x01\x00\x00")

func _1_initialize_schema_up_sql() ([]byte, error) {
	return bindata_read(
		__1_initialize_schema_up_sql,
		"1_initialize_schema.up.sql",
	)
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		return f()
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() ([]byte, error){
	"1_initialize_schema.down.sql": _1_initialize_schema_down_sql,
	"1_initialize_schema.up.sql": _1_initialize_schema_up_sql,
}
// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for name := range node.Children {
		rv = append(rv, name)
	}
	return rv, nil
}

type _bintree_t struct {
	Func func() ([]byte, error)
	Children map[string]*_bintree_t
}
var _bintree = &_bintree_t{nil, map[string]*_bintree_t{
	"1_initialize_schema.down.sql": &_bintree_t{_1_initialize_schema_down_sql, map[string]*_bintree_t{
	}},
	"1_initialize_schema.up.sql": &_bintree_t{_1_initialize_schema_up_sql, map[string]*_bintree_t{
	}},
}}
