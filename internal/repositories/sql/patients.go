package sql

import (
	"context"
	"fmt"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ppaysant/medical-demo/internal/domain"
	"gitlab.com/ppaysant/medical-demo/internal/ports"
)

type patientsRepository struct {
	db pgxconn
}

func (r *patientsRepository) GetPatientByID(ctx context.Context, ID string) (bool, domain.Patient, error) {
	patient := domain.Patient{}
	err := pgxscan.Get(ctx, r.db, &patient, `select "id", "name", "address", "birthdate" from "patients" where "id" = $1`, ID)
	if err != nil {
		if pgxscan.NotFound(err) {
			return false, patient, nil
		}
		return false, patient, fmt.Errorf("Failed to select all patients: %w", err)
	}
	return true, patient, nil
}

func (r *patientsRepository) GetAllPatients(ctx context.Context) ([]domain.Patient, error) {
	patients := []domain.Patient{}
	err := pgxscan.Select(ctx, r.db, &patients, `select "id", "name", "address", "birthdate" from "patients"`)
	if err != nil {
		return nil, fmt.Errorf("Failed to select all patients: %w", err)
	}
	return patients, nil
}

func (r *patientsRepository) CreatePatient(ctx context.Context, patient *domain.Patient) error {
	var id string
	err := pgxscan.Get(
		ctx,
		r.db,
		&id,
		`insert into "patients" ("name", "address", "birthdate") values ($1, $2, $3) returning "id"`,
		patient.Name,
		patient.Address,
		patient.Birthdate,
	)
	if err != nil {
		return fmt.Errorf("Failed to insert patient: %w", err)
	}
	patient.ID = id
	return nil
}

func (r *patientsRepository) UpdatePatient(ctx context.Context, patient domain.Patient) error {
	_, err := r.db.Exec(
		ctx,
		`update "patients" set "name" = $1, "address" = $2, "birthdate" = $3 where "id" = $4`,
		patient.Name,
		patient.Address,
		patient.Birthdate,
		patient.ID,
	)
	if err != nil {
		return fmt.Errorf("Failed to update patient: %w", err)
	}
	return nil
}

func (r *patientsRepository) DeletePatientByID(ctx context.Context, ID string) error {
	_, err := r.db.Exec(ctx, `delete from "patients" where "id" = $1`, ID)
	if err != nil {
		return fmt.Errorf("Failed to delete patient: %w", err)
	}
	return nil
}

func (r *patientsRepository) GetAllPatientsDiagnostics(ctx context.Context, patientID string) ([]domain.Diagnostic, error) {
	diagnostics := []domain.Diagnostic{}
	err := pgxscan.Select(
		ctx,
		r.db,
		&diagnostics,
		`select "id", "disease", "made_on" from "diagnostics" where "patient_id" = $1`,
		patientID,
	)
	if err != nil {
		return nil, fmt.Errorf("Failed to select all diagnostics: %w", err)
	}
	return diagnostics, nil
}

func (r *patientsRepository) CreatePatientsDiagnostic(ctx context.Context, patientID string, diagnostic *domain.Diagnostic) error {
	var id string
	err := pgxscan.Get(
		ctx,
		r.db,
		&id,
		`insert into "diagnostics" ("patient_id", "disease", "made_on") values ($1, $2, $3) returning "id"`,
		patientID,
		diagnostic.Disease,
		diagnostic.MadeOn,
	)
	if err != nil {
		return fmt.Errorf("Failed to insert disagnotic: %w", err)
	}
	diagnostic.ID = id
	return nil
}

// NewPatientsRepository return a new repository backed by a postgresql database
func NewPatientsRepository(db *pgxpool.Pool) ports.PatientsRepository {
	return &patientsRepository{
		db: db,
	}
}
