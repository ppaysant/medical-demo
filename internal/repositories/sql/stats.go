package sql

import (
	"context"
	"fmt"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ppaysant/medical-demo/internal/domain"
	"gitlab.com/ppaysant/medical-demo/internal/ports"
)

type statsRepository struct {
	db pgxconn
}

func (r *statsRepository) GetPatientsByDisease(ctx context.Context) ([]domain.Aggregate, error) {
	aggs := []domain.Aggregate{}
	err := pgxscan.Select(
		ctx,
		r.db,
		&aggs,
		`select "d"."disease" as "label", count(distinct "p"."id") as "value" 
		 from "diagnostics" as "d"
		 inner join "patients" as "p" on ("d"."patient_id" = "p"."id")
		 group by "d"."disease"
		 order by 2 desc,1`,
	)
	if err != nil {
		return aggs, fmt.Errorf("Failed to aggregate patients by disease: %w", err)
	}
	return aggs, nil
}

// NewStatsRepository return a new repository backed by a postgresql database
func NewStatsRepository(db *pgxpool.Pool) ports.StatsRepository {
	return &statsRepository{
		db: db,
	}
}
