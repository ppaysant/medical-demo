package domain

import "time"

type Patient struct {
	ID        string
	Name      string
	Address   string
	Birthdate time.Time
}
