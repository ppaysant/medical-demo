package domain

type Aggregate struct {
	Label string
	Value int
}
