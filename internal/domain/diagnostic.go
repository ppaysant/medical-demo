package domain

import "time"

type Diagnostic struct {
	ID      string
	Disease string
	MadeOn  time.Time
}
