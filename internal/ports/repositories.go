package ports

import (
	"context"

	"gitlab.com/ppaysant/medical-demo/internal/domain"
)

// PatientsRepository stores patients
type PatientsRepository interface {
	GetPatientByID(ctx context.Context, ID string) (ok bool, patient domain.Patient, err error)
	// GetAllPatients retrieve all patients. An empty slice must be returned when no patient exist.
	GetAllPatients(ctx context.Context) ([]domain.Patient, error)
	// Create adds a new patient in the repository. Patient ID must be assigned by the repository.
	CreatePatient(ctx context.Context, patient *domain.Patient) error
	UpdatePatient(ctx context.Context, patient domain.Patient) error
	DeletePatientByID(ctx context.Context, ID string) error

	GetAllPatientsDiagnostics(ctx context.Context, patientID string) ([]domain.Diagnostic, error)
	CreatePatientsDiagnostic(ctx context.Context, patientID string, diagnostic *domain.Diagnostic) error
}

type StatsRepository interface {
	GetPatientsByDisease(ctx context.Context) ([]domain.Aggregate, error)
}
