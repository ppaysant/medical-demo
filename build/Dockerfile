FROM golang:1.15.7 AS base
WORKDIR /build

FROM base AS deps
# We want to populate the module cache based on the go.{mod,sum} files.
COPY go.mod .
COPY go.sum .
RUN go mod download

FROM deps AS build-env
COPY . .

FROM build-env AS build
ENV GOOS=linux GOARCH=amd64

RUN make build

FROM gcr.io/distroless/base AS patients-api
WORKDIR /app
COPY --from=build /build/dist/bin/patients-api /app
COPY --from=build /build/dist/bin/migrate /app
CMD ["./patients-api"]

FROM gcr.io/distroless/base AS migrate
WORKDIR /app
COPY --from=build /build/dist/bin/migrate /app
CMD ["./migrate"]

FROM gcr.io/distroless/base AS stats-api
WORKDIR /app
COPY --from=build /build/dist/bin/stats-api /app
CMD ["./stats-api"]