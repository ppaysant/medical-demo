BIN_DIR=bin
OAPI_CODEGEN_BIN=$(BIN_DIR)/oapi-codegen
OAPI_CODEGEN_PKG=github.com/deepmap/oapi-codegen/cmd/oapi-codegen
GO_BINDATA_BIN=$(BIN_DIR)/go-bindata
GO_BINDATA_PKG=github.com/jteeuwen/go-bindata/...
MOCKGEN_BIN=$(BIN_DIR)/mockgen
MOCKGEN_PKG=github.com/golang/mock/mockgen

$(OAPI_CODEGEN_BIN): go.sum
	GOBIN=$(CURDIR)/$(BIN_DIR) go install $(OAPI_CODEGEN_PKG)

$(GO_BINDATA_BIN): go.sum
	GOBIN=$(CURDIR)/$(BIN_DIR) go install $(GO_BINDATA_PKG)

$(MOCKGEN_BIN): go.sum
	GOBIN=$(CURDIR)/$(BIN_DIR) go install $(MOCKGEN_PKG)

generate: $(OAPI_CODEGEN_BIN) $(GO_BINDATA_BIN) $(MOCKGEN_BIN)
	$(OAPI_CODEGEN_BIN) -generate types,server \
	  -package handlers \
	  -o internal/app/patients/handlers/api.gen.go \
	  api/patients.yaml
	$(OAPI_CODEGEN_BIN) -generate types,server \
	  -package handlers \
	  -o internal/app/stats/handlers/api.gen.go \
	  api/stats.yaml
	cd internal/repositories/sql/migrations; $(CURDIR)/$(GO_BINDATA_BIN) -pkg migrations \
	  -ignore=bindata.go \
	  -o bindata.go \
	  .
	$(MOCKGEN_BIN) -destination=internal/mocks/ports/repositories.gen.go -package=ports gitlab.com/ppaysant/medical-demo/internal/ports PatientsRepository

dist:
	@mkdir -p dist

dist/bin: dist
	@mkdir -p dist/bin

build: dist/bin
	go build -a -o dist/bin/ ./cmd/...

unit-test: generate
	go test ./cmd/... ./internal/...

integration-test: generate
	go test -count 1 ./test/integration/...

vet:
	go vet ./...

.PHONY: generate build unit-test vet