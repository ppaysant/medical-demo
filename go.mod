module gitlab.com/ppaysant/medical-demo

go 1.15

require (
	github.com/deepmap/oapi-codegen v1.4.2
	github.com/georgysavva/scany v0.2.7
	github.com/go-testfixtures/testfixtures/v3 v3.5.0
	github.com/golang-migrate/migrate/v4 v4.14.1
	github.com/golang/mock v1.4.4
	github.com/google/go-cmp v0.5.4 // indirect
	github.com/jackc/pgconn v1.8.0
	github.com/jackc/pgx/v4 v4.10.1
	github.com/jteeuwen/go-bindata v3.0.7+incompatible
	github.com/labstack/echo/v4 v4.1.11
	github.com/maxbrunsfeld/counterfeiter/v6 v6.3.0
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/ory/dockertest/v3 v3.6.3
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.6.1
	github.com/wcharczuk/go-chart/v2 v2.1.0
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
	golang.org/x/sys v0.0.0-20210113181707-4bcb84eeeb78 // indirect
	gotest.tools v2.2.0+incompatible
)
